const router = require("express").Router();
const cakesController = require("../controllers/cakes");
router.get("/cakes", cakesController.getCakes);
router.post("/cakes", cakesController.createCake);

module.exports = router;
