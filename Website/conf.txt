sudo ln -s /etc/nginx/sites-available/trainingwebsite /etc/nginx/sites-enabled/


server {
        listen 80;
        listen [::]:80;

server_name ip;
        root /var/www/test.com/html;
        index index.html;

        location / {
                try_files $uri $uri/ =404;
        }
}
